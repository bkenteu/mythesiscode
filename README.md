
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/brendankent/mythesiscode">
    <img src="https://gitlab.com/brendankent/mythesis/-/raw/master/10_05_2022_10_06_52_val_iou_per_class.png" alt="Logo" height="240">
  </a>

<h3 align="center">MSc Thesis Code</h3>

  <p align="center">
    Ensemble Approach to the Semantic Segmentation of Satellite Images
    <br />
    <br />
    <a href="https://gitlab.com/brendankent/mythesis/-/releases">View PDF Release</a>
    <!-- ·
    <a href="https://github.com/github_username/repo_name/issues">Report Bug</a>
    ·
    <a href="https://github.com/github_username/repo_name/issues">Request Feature</a> -->
  </p>
</div>

<!-- ABOUT THE PROJECT -->
## About The Thesis

# Project Title

Ensemble Approach to the Semantic Segmentation of Satellite Images

## Abstract

Automatic classification and segmentation of land use land cover(LULC) is extremely important for understanding the relationship between humans and nature. Human pressures on the environment have drastically accelerated in the last decades, risking biodiversity and ecosystem services. Remote sensing via satellite imagery is an excellent tool to study LULC.  
Research has shown that deep learning encoder-decoder architectures have achieved worthy results in the area of LULC, however the application of an ensemble approach has not been well quantified. Studies have shown it to be useful in the area of medical imaging. Ensembling by pooling together predictions to produce better predictions is a well known technique in machine learning.  
This study aims to quantify the statistical improvement that a deep learning ensemble approach can give to solving a semantic segmentation problem on satellite imagery. 

Building on existing state-of-the-art approaches to semantic segmentation, such as decoder-encoder architectures, data augmentation, transfer learning, this study asks: to what extent can an average or weighted average ensemble improve the intersection over union metric for a satellite image segmentation problem in comparison to a single base model? The intersection over union measures the similarity between the ground truth labelled images and those predicted by a deep learning model.  
Based on a review of the literature on semantic segmentation, a U-Net architecture with a ResNet-34 decoder was used to build an average and a weighted average ensemble. A land cover classification dataset presented by the DeepGlobe Challenge was then used for training and evaluating the models. 

The results have shown that an average ensemble gave a statistically greater intersection over union than a single average U-Net model, a statistical test found the effect to be very strong, however a weighted ensemble did not improve on the results of a simple average. 
The results provide evidence that an ensemble approach can achieve better segmentation classifications of LULC in satellite images by using an averaging technique, however the inference time of an ensemble needs to be taken into consideration, as this study showed a well selected single model can give results as good as an ensemble and has an advantage of a much faster inference time. 
Further research is needed to identify methods for model selection in ensemble deep learning in order to reduce the inference time.

## Getting Started

### Dependencies

* Python v3.7.13 

the following libraries:

* Pandas v1.4.1  for data manipulation
* Matplotlib v3.5.1  for data visualization 
* TensorFlow v2.8.0 for machine learning model building and experimentation, * Albumentations v1.1.0  for image augmentations
* Seaborn v0.11.2 (similiar to Matplotlib) 
* segmentation-models v1.0.1 for building pre-trained image segmentation based models

### Dataset

The data set comes from DeepGlobe 2018: A Challenge to Parse the Earth through Satellite Images (Demir et al., [2018](https://doi.org/10.1109/CVPRW.2018.00031)). 

<img src="https://gitlab.com/brendankent/mythesis/-/raw/master/example_image_mask.png" height="240">

### Phase 1 Create models

use [Create_Models.ipynb](Create_Models.ipynb)

### Phase 2 Build ensembles and analyse results

use [Analysis_models_v2.py](Analysis_models_v2.py) to test models, build average ensemble and weighted ensemble

use [Simple_mnist_example.py](Simple_mnist_example.py) to test out the weighted enseble on a simplier data set, like for image classifcation of mnist numbers

[Plot_charts.py](Plot_charts.py) used to mess about with plotting 

directory [workshop](workshop) is where the understanding of albumentations and IoU was done

[html_output](html_output) is where some of the saved outputs from the evaluations are kept

[put_away.csv](put_away.csv) is the data set which separated from the main DeepGlobe data at the beginning of the study

## saved models from phase 1

Google Drive [DEEP_LEARNING_SATELLITE](https://drive.google.com/drive/folders/1dfEKdJ3sEK0WUq-3RdR1t9IFM-UiB4wl?usp=sharing) Folder containing all the 64 models which were built 
