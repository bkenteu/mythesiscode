#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 19:23:20 2022

"""

import sys 
import os
import math
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import segmentation_models as sm
import albumentations as A
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pickle
from datetime import datetime
sys.path.append("/home/kent/college/Thesis/myThesisCode/")
from Create_models_v2 import load_model, load_original_dataset, load_original_classes, plot_history_from_folder
from Create_models_v2 import SatelliteAugmentationGenerator, SatellitePatchesGenerator, get_preprocessing, reverse_one_hot
from Create_models_v2 import plot_logger_from_folder, SAVE_PATH, EPOCHS, SAMPLES, STOPPING_PATIENCE, split_dataset, get_augmentation_for_train
from Create_models_v2 import DATASET_DIR, Patches, make_test_patches, SatelliteTestPatchesGenerator, drop_the_put_away, get_augmentation_for_val
sm.set_framework('tf.keras')
sm.framework()
    
def get_random_test_set(samples=10):
    metadata_df = load_original_dataset() 
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    test_df = metadata_df.sample(samples)
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    test_gen = SatelliteAugmentationGenerator(
        batch_size=8, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=test_df, 
        augmentation=get_augmentation_for_val(), # random crop
        preprocessing=get_preprocessing(preprocess_input),
    )
    return test_gen

def get_put_away_test_set():
    """returned a sequence generator of the full test set"""
    # test_df = pd.read_csv(os.path.join(DATASET_DIR, 'put_away.csv'))
    test_df = pd.read_csv(os.path.join(DATASET_DIR, 'put_away_patches.csv'))
    test_df = test_df.sample(8)
    print("test_df size:", len(test_df))
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    test_gen = SatelliteTestPatchesGenerator(
        satellite_class_rgb_values=satellite_class_rgb_values,
        batch_size=8,
        data=test_df, 
        preprocessing=get_preprocessing(preprocess_input),
    )   
    return test_gen

def get_divided_test_set(sections):
    """returned a sequence generator of the full test set
    divided into sections"""
    test_df = pd.read_csv(os.path.join(DATASET_DIR, 'put_away_patches.csv'))
    # test_df = pd.read_csv(os.path.join(DATASET_DIR, 'put_away.csv'))
    print("test_df size:", len(test_df))
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    test_gens = []
    for ts in np.array_split(test_df, sections):
        test_gens.append(SatelliteTestPatchesGenerator(
            satellite_class_rgb_values=satellite_class_rgb_values,
            batch_size=8,
            data=ts, 
            preprocessing=get_preprocessing(preprocess_input),
        ))
    print("test_gens", len(test_gens))
    return test_gens

def inspect_one_test_image_mask(test_gen, number):
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    image  = test_gen[0][0][number]
    mask = test_gen[0][1][number]
    mask_class = satellite_class_rgb_values[np.argmax(mask, axis=-1).squeeze()]
    plt.imshow(image)
    plt.show()
    plt.imshow(mask_class)
    plt.show()
    
def load_all_models(models_dir, limit=0, plot=False):
    models = []
    for model_dir in os.listdir(models_dir):
        if "2022" not in model_dir:
            continue
        if os.path.exists(os.path.join(models_dir, model_dir,"best_model.h5")):
            print(model_dir)
            m = load_model(os.path.join(models_dir, model_dir,"best_model.h5"))
            m._name = model_dir
            models.append(m)
            if plot:
                plot_logger_from_folder(os.path.join(models_dir, model_dir))
        if limit:
            if len(models) == limit:
                return models            
    return models
   
def inspect_all_models(models_dir):
    total_epochs = []
    for model_dir in os.listdir(models_dir):
        if os.path.exists(os.path.join(models_dir, model_dir,"best_model.h5")):
            print(model_dir)
            history = pd.read_csv(os.path.join(models_dir, model_dir, 'logger.csv'))
            total_epochs.append(len(history) )
    import statistics 
    print(f"Mean of the models is {statistics.mean(total_epochs)}") 
    print(f"Standard Deviation of the models is {statistics.stdev(total_epochs)}") 


def evaluate_all_models(test_gen, models):
    """Return a DataFrame of all the models which contains the metrics"""
    d = []
    for m in models:
        print("Name:", m.name)
        print("Evaluate:")
        scores = m.evaluate(test_gen)
        scores_dict = {"name": m.name}
        scores_dict.update(dict(zip(m.metrics_names,scores)))
        d.append(scores_dict)
    return pd.DataFrame(d)
        
def evaluate_model_on_divided_test_gens(model, test_gens):
    """Return a DataFrame of a model with all test_gen splits"""
    d = []
    for i, test_gen in enumerate(test_gens):
        print("Evaluate:")
        scores = model.evaluate(test_gen)
        scores_dict = {"testset": i}
        scores_dict.update(dict(zip(model.metrics_names,scores)))
        d.append(scores_dict)
    return pd.DataFrame(d)
        
          
        
def combine_model_average(test_gen, models, sample_num):
    predictions = []
    for m in models:
        print("Name:", m.name)
        print("Generate predictions")
        preds = m.predict(test_gen)
        print("predictions shape:", preds.shape)
        predictions.append(preds)
    predictions = np.stack(predictions)
    
    print("All together:")
    print(predictions.shape)
    
    print("Mean:")
    mean_predictions = np.mean(predictions, axis=(0))
    print(mean_predictions.shape)
    real_mask = reverse_one_hot(test_gen[0][1][sample_num,:,:,:])
    prediction_mask = reverse_one_hot(mean_predictions[sample_num,:,:,:])
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    colors_real_mask = satellite_class_rgb_values[real_mask].squeeze()
    colors_prediction_mask = satellite_class_rgb_values[prediction_mask].squeeze()
    visualize(
        image=test_gen[0][0][sample_num,:,:,:]*255, 
        real_mask=colors_real_mask,
        prediction_mask=colors_prediction_mask
    )
    
    print("before argmax:")
    iou = tf.keras.metrics.MeanIoU(num_classes=7)
    iou.update_state(np.reshape(test_gen[0][1], -1),
                      np.reshape(mean_predictions, -1))
    print("IoU:", iou.result().numpy())
    mean_predictions_ints = np.argmax(mean_predictions, axis = -1)
    test_gen_ints = np.argmax(test_gen[0][1], axis = 1)
    
    print("with argmax:")
    # Computes the mean Intersection-Over-Union metric.
    # https://www.tensorflow.org/api_docs/python/tf/keras/metrics/MeanIoU
    iou = tf.keras.metrics.MeanIoU(num_classes=7)
    iou.update_state(np.reshape(test_gen_ints, -1),
                      np.reshape(mean_predictions_ints, -1))
    print("IoU:", iou.result().numpy())
    
# helper function for data visualization
def visualize(**images):
    """PLot images in one row."""
    n = len(images) + 1
    plt.figure(figsize=(12, 8))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title() +"\n" +str(image.shape))
        plt.imshow(image.astype("uint8"))
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    plt.subplot(1, n, i + 2)
    plt.xticks([])
    plt.yticks([])
    legend_colours = []
    for i in range(7):
        legend_colours.append(mpatches.Patch(color=satellite_class_rgb_values[i]/255, label=satellite_class_list[i]))
    plt.legend(handles=legend_colours)
    plt.axis('off')
    plt.title(' ')
    plt.imshow(np.full((512,512, 3),255))
    plt.show()
    
def display_sample_of_cropping():
    """get one iamge and divied it to show images and masks"""
    df = load_original_dataset()
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    
    sample = df.sample(1) #'image id 513968'
    
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["sat_image_path"].values[0]), dtype="uint8")
 
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["mask_path"].values[0]), dtype="uint8")
        
    visualize(
        image=image,
        mask=mask
    )
    
    tf_img = tf.convert_to_tensor(np.asarray([image,mask]))
    patches = Patches(512)(tf_img)
    p_images = []
    p_masks = []
    for p_num in range(16):
        patch_img = tf.reshape(patches[0][p_num], (512, 512) + (3,)).numpy()
        patch_mask = tf.reshape(patches[1][p_num], (512, 512) + (3,)).numpy()
        p_images.append(patch_img)
        p_masks.append(patch_mask)
        
        
    visualize(
        image=image,
        mask=mask
    )
        
    plt.figure(figsize=(8, 8))
    all_imgs = p_images[:4] + p_masks[:4] + p_images[4:8] + p_masks[4:8] + p_images[8:12] + p_masks[8:12] + p_images[12:] + p_masks[12:]
    for i in range(1,33):
        ax = plt.subplot(8, 8, i)
        plt.imshow(all_imgs[i-1])
        plt.axis("off")
    plt.show()
    
    plt.figure(figsize=(4, 4))
    n = 4
    i = 1
    for pic in p_masks:
        ax = plt.subplot(n, n, i)
        plt.imshow(pic)
        plt.axis("off")
        i += 1
    plt.show()
    
        
def display_sample(test_gen, preds, sample_nums, onehotview=False, model_name=""):
    """Quick utility to display a model's prediction."""
    for sample_num in sample_nums:
        prediction_mask = np.argmax(preds[sample_num], axis=-1)
        prediction_mask = np.expand_dims(prediction_mask, axis=-1)
        real_mask = reverse_one_hot(test_gen[0][1][sample_num,:,:,:])
        real_mask = np.expand_dims(real_mask, axis=-1)
        
        satellite_class_list, \
        satellite_class_indices, \
        satellite_class_rgb_values = load_original_classes()
        
        colors_real_mask = satellite_class_rgb_values[real_mask].squeeze()
        colors_prediction_mask = satellite_class_rgb_values[prediction_mask].squeeze()
    
        print(model_name)
        if model_name == "model_3":
            print("base_avg")
        if model_name == "model_9":
            print("base_max")
        visualize(
            image=test_gen[0][0][sample_num,:,:,:]*255, 
            real_mask=colors_real_mask,
            prediction_mask=colors_prediction_mask
        )


    # if onehotview:
    #     plt.figure(figsize=(15, 5))
    #     num_classes = len(satellite_class_list)
    #     plot_number = 1
    #     for i in range(num_classes-1):
    #         level = np.zeros((512,512,1), dtype="int64") + i
    #         prediction_level = (prediction_mask == level).all(axis=2,  keepdims=True)
    #         real_mask_level = (real_mask == level).all(axis=2,  keepdims=True)
    #         plt.subplot(2, 6, plot_number)
    #         plt.xticks([])
    #         plt.yticks([])
    #         plt.title(satellite_class_list[i] + " (True)")
    #         plt.imshow(real_mask_level, cmap="binary")
    #         plot_number += 1
    #         plt.subplot(2, 6, plot_number)
    #         plt.xticks([])
    #         plt.yticks([])
    #         plt.title(satellite_class_list[i] + " (Pred)")
    #         plt.imshow(prediction_level, cmap="binary", alpha=0.8)
    #         plot_number += 1
    #     plt.show()
    
    
def display_sample_range(test_gen, preds, n):
    for sample_num in range(n):
        display_sample(test_gen, preds, sample_num)

        
def compare_models_display_sample(test_gen, models, sample_nums):
    for m in models:
        print(m.name)
        preds = m.predict(test_gen)
        display_sample(test_gen, preds, sample_nums, onehotview=False, model_name=m.name)
        
def histogram_of_results(df):
    std = np.std(df["onehot_mean_iou"])
    mean = np.average(df["onehot_mean_iou"])
    plt.style.use('seaborn-whitegrid')   
    plt.rcParams['figure.dpi'] = 90
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams.update({'font.size': 13})
    plt.rcParams['figure.figsize'] = 8.3,5.8
    fig, ax = plt.subplots(figsize=(12,7))
    # plt.xlim([min(data)-5, max(data)+5])
    plt.hist(df["onehot_mean_iou"], bins=20,density=True, facecolor='g', alpha=0.75)
    plt.text(.475, 15, r'$\mu=' +str(np.round(mean,2))+ ',\ \sigma=$'+ str(np.round(std,2)))
    plt.ylabel('Count of models')
    plt.xlabel('mIoU (one-hot mean IoU)')
    # plt.title('Total number of Pixels in 803 images per label in Billions')
    plt.show()
    
    import seaborn as sns
    # import matplotlib.pyplot as plt
    sns.set(style="whitegrid")
    fig, ax = plt.subplots()
    fig.set_size_inches(8.3, 5.8)
    sns.histplot(ax=ax, data=df, x="onehot_mean_iou", color="green", label="onehot_mean_iou", kde=True, bins=10)
    # ax.text(.425, 12, r'$\mu=' +str(np.round(mean,3))+ ',\ \sigma=$'+ str(np.round(std,3)), size = 14)
    # # sns.despine(left=True)
    ax.set_xlabel("mIoU (one-hot mean IoU)", size = 14, alpha=1)
    ax.set_ylabel("Count of models", size = 14, alpha=1)
    # plt.legend() 
    fig.tight_layout()
    plt.show()

    df = df.sort_values('onehot_mean_iou', ascending=True)
    import seaborn as sns
    sns.set(style="whitegrid")
    fig, ax = plt.subplots()
    sns.barplot(data=df, y="name", x= "onehot_mean_iou", color="red", label="onehot_mean_iou", alpha=0.75)
    # ax.tick_params(axis='x', rotation=90)
    # sns.despine(left=True)
    ax.set_xlabel("mIoU (one-hot mean IoU)", size = 14, alpha=1)
    ax.set_ylabel("Model name", size = 14, alpha=1)
    plt.xlim(0.3, None)
    # plt.legend() 
    plt.show()
        
    
def get_ensemble_model(models):
    optim = tf.keras.optimizers.Adam(0.001)
    
    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    
    # metrics = [tf.keras.metrics.MeanIoU(num_classes=7),
    #            sm.metrics.IOUScore(threshold=0.5, per_image=True),
    #            sm.metrics.FScore(threshold=0.5)]
    # https://segmentation-models.readthedocs.io/en/latest/api.html
    model_input = tf.keras.Input(shape=(512, 512, 3)) #takes a list of tensors as input, all of the same shape
    model_outputs = [model(model_input) for model in models] #collects outputs of models in a list
    ensemble_output = tf.keras.layers.Average()(model_outputs) #averaging outputs
    ensemble_model = tf.keras.Model(inputs=model_input, outputs=ensemble_output)
    ensemble_model._name = "ensemble_model"

    ensemble_model.trainable = False
    # ensemble_model.compile(optim, total_loss, metrics)
    ensemble_model.compile(optimizer=optim,
              loss=total_loss,
              metrics=[tf.keras.metrics.CategoricalAccuracy(name="categorical_accuracy", dtype=None),
                       tf.keras.metrics.OneHotMeanIoU(name="onehot_mean_iou", num_classes=7),
                       # tf.keras.metrics.MeanIoU(name="mean_iou", num_classes=7),
                       tf.keras.metrics.OneHotIoU(name="iou_0", num_classes=7, target_class_ids=[0]),
                       tf.keras.metrics.OneHotIoU(name="iou_1",num_classes=7, target_class_ids=[1]),
                       tf.keras.metrics.OneHotIoU(name="iou_2",num_classes=7, target_class_ids=[2]),
                       tf.keras.metrics.OneHotIoU(name="iou_3",num_classes=7, target_class_ids=[3]),
                       tf.keras.metrics.OneHotIoU(name="iou_4",num_classes=7, target_class_ids=[4]),
                       tf.keras.metrics.OneHotIoU(name="iou_5",num_classes=7, target_class_ids=[5]),
                       tf.keras.metrics.OneHotIoU(name="iou_6",num_classes=7, target_class_ids=[6]),
                       sm.metrics.IOUScore(name="sm_iou_score", threshold=0.5, per_image=True),
                       # tf.keras.metrics.IoU(num_classes=2, target_class_id=[0])
                       ])

    ensemble_model.summary(show_trainable=True)
    # ensemble_model.evaluate(test_gen)
    # preds_ensemble = ensemble_model.predict(test_gen)
    return ensemble_model

def print_model_details(model):
    print("^"*80)
    x = 20
    print(f"base_max")
    print(f"   {'layer.name'[:x].ljust(x)} {'layer.__class__.__name__'[:x].ljust(x)} {'layer.trainable'[:x].ljust(x)}\
 {'layer.count_params'[:x].ljust(x)} Other details")
    print(f"   {'----------'[:x].ljust(x)} {'--------------------'[:x].ljust(x)} {'---------------'[:x].ljust(x)}\
 {'------------------'[:x].ljust(x)} ------------------")
    for layer in model.layers:
        if layer.__class__.__name__ == "Conv2D":
            print(f"   {layer.name[:x].ljust(x)} {layer.__class__.__name__[:x].ljust(x)} {str(layer.trainable)[:x].ljust(x)}\
 {str(layer.count_params())[:x].ljust(x)}  filters:{str(layer.filters)[:x].ljust(x)} kernel_size:{str(layer.kernel_size)[:x].ljust(x)} ")
        elif layer.__class__.__name__ == "UpSampling2D":
           print(f"   {layer.name[:x].ljust(x)} {layer.__class__.__name__[:x].ljust(x)} {str(layer.trainable)[:x].ljust(x)}\
 {str(layer.count_params())[:x].ljust(x)} size: {str(layer.size)[:x].ljust(x)} ")
        else:
            print(f"   {layer.name[:x].ljust(x)} {layer.__class__.__name__[:x].ljust(x)} {str(layer.trainable)[:x].ljust(x)}\
 {str(layer.count_params())[:x].ljust(x)}")
    print("^"*80)
      
class WeightedAverage(tf.keras.layers.Layer):
    """taken from https://stackoverflow.com/q/62595660"""    
    def __init__(self):
        super(WeightedAverage, self).__init__()
        
    def build(self, input_shape):
        
        self.W = self.add_weight(
                    shape=(1,1,len(input_shape)),
                    initializer='uniform',
                    dtype=tf.float32,
                    trainable=True)
        
    def get_config(self):
        config = super(WeightedAverage, self).get_config()
        return config

    def call(self, inputs):

        # inputs is a list of tensor of shape [(n_batch, n_feat), ..., (n_batch, n_feat)]
        # expand last dim of each input passed [(n_batch, n_feat, 1), ..., (n_batch, n_feat, 1)]
        inputs = [tf.expand_dims(i, -1) for i in inputs]
        inputs = tf.keras.layers.Concatenate(axis=-1)(inputs) # (n_batch, n_feat, n_inputs)
        weights = tf.nn.softmax(self.W, axis=-1) # (1,1,n_inputs)
        # weights sum up to one on last dim

        return tf.reduce_sum(weights*inputs, axis=-1) # (n_batch, n_feat) 


def get_w_ensemble_model(models):
    model_name = "w_ensemble_model"
    optim = tf.keras.optimizers.Adam(0.001)
    
    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    
    # metrics = [tf.keras.metrics.MeanIoU(num_classes=7),
    #            sm.metrics.IOUScore(threshold=0.5, per_image=True),
    #            sm.metrics.FScore(threshold=0.5)]
    # https://segmentation-models.readthedocs.io/en/latest/api.html
    model_input = tf.keras.Input(shape=(512, 512, 3)) #takes a list of tensors as input, all of the same shape
    model_outputs = [model(model_input) for model in models] #collects outputs of models in a list
    w_ensemble_output = WeightedAverage()(model_outputs) #averaging outputs
    w_ensemble_model = tf.keras.Model(inputs=model_input, outputs=w_ensemble_output)
    w_ensemble_model._name = model_name
    w_ensemble_model.trainable = False


    # ensemble_model.compile(optim, total_loss, metrics)
    w_ensemble_model.compile(optimizer=optim,
              loss=total_loss,
              metrics=[tf.keras.metrics.CategoricalAccuracy(name="categorical_accuracy", dtype=None),
                       tf.keras.metrics.OneHotMeanIoU(name="onehot_mean_iou", num_classes=7),
                       # tf.keras.metrics.MeanIoU(name="mean_iou", num_classes=7),
                       tf.keras.metrics.OneHotIoU(name="iou_0", num_classes=7, target_class_ids=[0]),
                       tf.keras.metrics.OneHotIoU(name="iou_1",num_classes=7, target_class_ids=[1]),
                       tf.keras.metrics.OneHotIoU(name="iou_2",num_classes=7, target_class_ids=[2]),
                       tf.keras.metrics.OneHotIoU(name="iou_3",num_classes=7, target_class_ids=[3]),
                       tf.keras.metrics.OneHotIoU(name="iou_4",num_classes=7, target_class_ids=[4]),
                       tf.keras.metrics.OneHotIoU(name="iou_5",num_classes=7, target_class_ids=[5]),
                       tf.keras.metrics.OneHotIoU(name="iou_6",num_classes=7, target_class_ids=[6]),
                       sm.metrics.IOUScore(name="sm_iou_score", threshold=0.5, per_image=True),
                       # tf.keras.metrics.IoU(num_classes=2, target_class_id=[0])
                       ])

    for layer in w_ensemble_model.layers:
        if "weighted_average" in layer.name:
            layer.trainable = True             
        else:
            layer.trainable = False 

    w_ensemble_model.summary(show_trainable=True) 
    print_model_details(w_ensemble_model)
    
    metadata_df = load_original_dataset()
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()

    df = drop_the_put_away(metadata_df)
    
    df = df.sample(100) # to make testing faster
    
    # Initial train and test split.
    train_df, val_df = train_test_split(
        df,
        test_size=0.20,
        shuffle=True
    )
    print("train_df",  train_df.shape)
    print("val_df",  val_df.shape)
    
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    train_gen = SatelliteAugmentationGenerator(
        batch_size=10, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=train_df, 
        augmentation=get_augmentation_for_val(),
        preprocessing=get_preprocessing(preprocess_input),
    )

    val_gen = SatelliteAugmentationGenerator(
        batch_size=10, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        data=val_df, 
        augmentation=get_augmentation_for_val(),
        preprocessing=get_preprocessing(preprocess_input),
    )

    if not os.path.exists(os.path.join(SAVE_PATH, model_name)):
        os.mkdir(os.path.join(SAVE_PATH, model_name))
    full_directory = os.path.join(SAVE_PATH, model_name)
    callbacks = [
        tf.keras.callbacks.ModelCheckpoint(os.path.join(full_directory, "best_model.h5"), 
                            mode='max', save_best_only=True, verbose=1, monitor='val_onehot_mean_iou'),
        tf.keras.callbacks.CSVLogger(os.path.join(full_directory, "logger.csv"), append=True),
        tf.keras.callbacks.EarlyStopping(monitor="val_onehot_mean_iou", patience=2, verbose=1),
    ]

    history = w_ensemble_model.fit(
        train_gen, 
        epochs=6, 
        validation_data=val_gen,
    )
    
    with open(os.path.join(full_directory, "history.pickle"), 'wb') as f:
        pickle.dump(history.history, f)
        
    plot_history_from_folder(full_directory)
    return w_ensemble_model

        
def sample_model():
    inp1 = tf.keras.layers.Input((100,))
    inp2 = tf.keras.layers.Input((100,))
    x1 = tf.keras.layers.Dense(32, activation='relu')(inp1)
    x2 = tf.keras.layers.Dense(32, activation='relu')(inp2)
    W_Avg = WeightedAverage()([x1,x2])
    out = tf.keras.layers.Dense(1)(W_Avg)
    
    m = tf.keras.Model([inp1,inp2], out)
    m.compile('adam','mse')
    m.summary()
    
    tf.keras.utils.plot_model(m, to_file='model_1.png', show_shapes=True)

    n_sample = 1000
    X1 = np.random.uniform(0,1, (n_sample,100))
    X2 = np.random.uniform(0,1, (n_sample,100))
    y = np.random.uniform(0,1, (n_sample,1))
    
    m.fit([X1,X2], y, epochs=10)

            
def main():
    models_dir = "/home/kent/college/Thesis/save"
    models = load_all_models(models_dir, limit=0, plot=False)
    
    
    test_patches_df = make_test_patches()
    
    
    test_gen = get_random_test_set(samples=8*20)
    
    
    test_put_away = get_put_away_test_set()
    
    
    test_put_away_gen = get_divided_test_set(sections=40)
    # sample_num = 5
    
    for i in range(10):
        test_put_away = get_put_away_test_set()
        compare_models_display_sample(test_put_away, [base_model_1,base_model_2, ensemble_model], sample_nums=list(range(8)))
    
  
    # compare_models_display_sample(test_gen, [ensemble_model], sample_num)
    model_results_df = evaluate_all_models(test_gen, models) 
    model_results_df.to_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                          "models_results_df.csv"))
    
    # model_results_df = evaluate_all_models(test_put_away, models) 

    # model_results_df = evaluate_all_models(test_gen, models + [ensemble_model]) 

    
    base_model_1 = load_model(os.path.join(models_dir, "17_05_2022_08_29_54","best_model.h5"))
    base_model_2 = load_model(os.path.join(models_dir, "30_04_2022_01_28_23","best_model.h5"))
    # evaluate_all_models(test_put_away, [best_model]) 
    base_model_results_df_avg = evaluate_model_on_divided_test_gens(base_model, test_put_away_gen)    
    base_model_results_df_avg.to_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                          "base_model_results_df_avg.csv"))
    # combine_model_average(test_gen, models, sample_num)  
    
    ensemble_model = get_ensemble_model(models)    
    ensemble_model.save(os.path.join("/home/kent/college/Thesis/save/ensemble_model",
                                          "best_model.h5"))
    ensemble_model = load_model(os.path.join("/home/kent/college/Thesis/save/ensemble_model",
                                          "best_model.h5"))
    # evaluate_all_models(test_put_away, [ensemble_model]) 

    ensemble_model_results_df = evaluate_model_on_divided_test_gens(ensemble_model, test_put_away_gen) 
    ensemble_model_results_df.to_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                          "ensemble_model_results_df.csv"))
    
    # w_ensemble_model = get_w_ensemble_model(models)
    
    # filters models, take only the top 20 
    sample_names = model_results_df.sample(4)['name'].tolist()
    four_models = []
    for m in models:
        print(f"{m.name}")
        if m.name in sample_names:
            print(f"{m.name} is chosen")
            four_models.append(m)
    w_ensemble_model = get_w_ensemble_model(four_models) 
    w_ensemble_model_results_df = evaluate_model_on_divided_test_gens(w_ensemble_model, test_put_away_gen) 
    w_ensemble_model_results_df.to_csv(os.path.join("/home/kent/college/Thesis/analysis",
                                          "w_ensemble_model_results_df.csv"))
    # evaluate_all_models(test_put_away, [w_ensemble_model]) 
    
    # change BATCH TO BE FULL EPOCH 
    # change BATCH TO BE FULL EPOCH 
    # change BATCH TO BE FULL EPOCH 
    # change BATCH TO BE FULL EPOCH 
    # change BATCH TO BE FULL EPOCH 
    # change BATCH TO BE FULL EPOCH 
    tf.nn.softmax(w_ensemble_model.get_weights()[-1]).numpy()
    evaluate_all_models(test_gen, models + [ensemble_model, w_ensemble_model]) 
    
    
if __name__ == '__main__':
    pass
    # main()
    
        