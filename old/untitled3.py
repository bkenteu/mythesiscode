#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 00:27:35 2022

@author: kent
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 20:12:40 2022

@author: kent

1st phase

Data preparation split into Train, Validation, Test.

"""

import matplotlib.pyplot as plt
import os
from PIL import Image
import numpy as np
import pandas as pd
from tensorflow.keras.utils import load_img, img_to_array
import random

DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"

metadata_df = pd.read_csv(os.path.join(DATASET_DIR, 'metadata.csv'))
metadata_df = (metadata_df
                .query('split in ["train"]')
                )
metadata_df['sat_image_path'] = metadata_df['sat_image_path'].apply(lambda img_pth: os.path.join(DATASET_DIR, img_pth))
metadata_df['mask_path'] = metadata_df['mask_path'].apply(lambda img_pth: os.path.join(DATASET_DIR, img_pth))

from tensorflow.keras import layers
import numpy as np
from tensorflow.keras.preprocessing.image import load_img
import tensorflow as tf

class Patches(layers.Layer):
    def __init__(self, patch_size):
        super(Patches, self).__init__()
        self.patch_size = patch_size

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches
    
    
import matplotlib.pyplot as plt

image_size = 2448  # We'll resize input images to this size
patch_size = 256  # Size of the patches to be extract from the input images


num_patches = (image_size // patch_size) ** 2

# plt.figure(figsize=(4, 4))
image = img_to_array(load_img("/home/kent/college/Thesis/deepglobe/train/147545_sat.jpg"))
mask = img_to_array(load_img("/home/kent/college/Thesis/deepglobe/train/147545_mask.png"))
plt.imshow(image.astype("uint8"))
plt.axis("off")
plt.imshow(mask.astype("uint8"))
plt.axis("off")
# resized_image = tf.image.resize(
#     tf.convert_to_tensor([image]), size=(image_size, image_size)
# )
# resized_image = np.expand_dims([image], axis=0)
images = tf.convert_to_tensor([image,mask])

patches = Patches(patch_size)(images)
print(f"Image size: {image_size} X {image_size}")
print(f"Patch size: {patch_size} X {patch_size}")
print(f"Patches per image: {patches.shape[1]}")
print(f"Elements per patch: {patches.shape[-1]}")

n = int(np.sqrt(patches.shape[1]))
# plt.figure(figsize=(4, 4))
plt.figure(figsize=(10, 10))
for i, patch in enumerate(patches[0]):
    ax = plt.subplot(n, n, i + 1)
    patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
    plt.imshow(patch_img.numpy().astype("uint8"))
    plt.axis("off")
# plt.show()
# plt.imshow(patch_img.numpy().astype("uint8"))
plt.figure(figsize=(10, 10))
for i, patch in enumerate(patches[1]):
    ax = plt.subplot(n, n, i + 1)
    patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
    plt.imshow(patch_img.numpy().astype("uint8"))
    plt.axis("off")
plt.show()
# plt.imshow(patch_img.numpy().astype("uint8"))


# for index, row in metadata_df.iterrows():
#     print(index, row["sat_image_path"], row["mask_path"])
    
    # print(f"Image size: {image.shape}")
    # print(f"Mask size: {mask.shape}")
    # print(f"Patch size: {patch_size} X {patch_size}")
    # print(f"Patches per image: {all_patches.shape[1]}")
    # print(f"Elements per patch: {all_patches.shape[-1]}")
    
    
import cv2
image = cv2.imread('/home/kent/college/Thesis/deepglobe/patches/100694_00_sat.png')
mask = cv2.imread('/home/kent/college/Thesis/deepglobe/patches/100694_00_mask.png', cv2.IMREAD_GRAYSCALE)
image2 = tf.keras.utils.img_to_array(tf.keras.utils.load_img("/home/kent/college/Thesis/deepglobe/patches/100694_00_sat.png"), dtype="uint8")
mask2 = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                "/home/kent/college/Thesis/deepglobe/patches/100694_00_mask.png", color_mode="grayscale"),dtype="uint8")

mask2 = tf.keras.utils.img_to_array(tf.keras.utils.load_img("/home/kent/college/Thesis/deepglobe/train/147545_mask.png"))

# create the histogram
histogram, bin_edges = np.histogram(image, bins=256, range=(0, 1))
# configure and draw the histogram figure
plt.figure()
plt.title("Grayscale Histogram")
plt.xlabel("grayscale value")
plt.ylabel("pixel count")
# plt.xlim([0.0, 5.0])  # <- named arguments do not work here

plt.plot(bin_edges[0:-1], histogram)  # <- or here
plt.show()


#function to plot histogram
def histogram(img):
  pixels=[]
  #create list of values 0-255
  for x in range(256):
    pixels.append(x)
  #initialize width and height of image
  width,height=img.size
  counts=[]
  #for each intensity value
  for i in pixels:
    #set counter to 0
    temp=0
    #traverse through the pixels
    for x in range(width):
      for y in range(height):
        #if pixel intensity equal to intensity level
        #increment counter
        if (img.getpixel((x,y))==i):
          temp=temp+1
    #append frequency of intensity level 
    counts.append(temp)
  #plot histogram
  plt.bar(pixels,counts)
  plt.show()


d = {'image_id': {257: 632489}, 'split': {257: 'train'}, 'sat_image_path': {257: '/home/kent/college/Thesis/deepglobe/train/632489_sat.jpg'}, 'mask_path': {257: '/home/kent/college/Thesis/deepglobe/train/632489_mask.png'}, 'sat_image_patch': {257: '/tmp/patches/632489_14_sat.png'}, 'mask_patch': {257: '/tmp/patches/632489_14_mask.png'}}

sample = pd.DataFrame(d)
image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
    sample["sat_image_patch"].values[0]), dtype="uint8")
mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
        sample["mask_patch"].values[0]), dtype="uint8")
forest = np.zeros((256,256) + (3,), dtype="uint8") + (255,255,0)
mask_forest = (mask == forest).all(axis=2, keepdims=True).astype(int)
# img_reshape = mask_forest.reshape(-1,3)
unique, counts  = np.unique(mask_forest.reshape(-1), axis=0, return_counts=True)
# select_class_dict = get_class_dict()
uniques = [str(x) for x in unique] 
plt.figure(figsize=(12, 8))
plt.bar(uniques,counts)
plt.ylabel('number of pixels')
plt.xlabel('Classififcation')
plt.title('images')
plt.show()
    