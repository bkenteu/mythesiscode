# -*- coding: utf-8 -*-
# from google.colab import drive
# drive.mount('/content/drive')

# !pip install --upgrade pip
# !pip install opencv-python-headless==4.1.2.30
# !pip install -U albumentations
# !pip install segmentation-models

"""
1st phase

Data preparation by cutting images into many parts/patches.


2nd phase

Data preparation split into Train, Validation, Test
and train CNN model

T    - V    - T
80 % - 10 % - 10%

CNN: Unet
"""
                                    
import os
import numpy as np
import pandas as pd
import tensorflow as tf
from tqdm import tqdm
import os
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
import numpy as np
import segmentation_models as sm
import albumentations as A
import time
import matplotlib.pyplot as plt
import pickle

sm.set_framework('tf.keras')
sm.framework()
    

class Patches(tf.keras.layers.Layer):
    def __init__(self, patch_size):
        super(Patches, self).__init__()
        self.patch_size = patch_size

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches
    
    
def make_image_cuts(image_id, image_path, target_path, save_dir):
    patch_size = 256  # Size of the patches to be extract from the input images
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(image_path))
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(target_path))
    # forest = np.zeros((mask.shape[0],mask.shape[1]) + (3,), dtype="uint8") + (0,255,0)
    # mask_forest = (mask == forest).all(axis=2,  keepdims=True).astype(int)
    # unique, counts  = np.unique(mask_forest.reshape(-1), axis=0, return_counts=True)
    # uc_dict = dict(zip(unique, counts))

    # if 1 not in uc_dict:
    #     print("\nbad", uc_dict)
    #     return [], []
    # if uc_dict[1] < 10000:
    #     print("\nbad", uc_dict)
    #     return [], []
    # print("\ngood", uc_dict)
    # images = tf.convert_to_tensor([image,mask])
    images = tf.convert_to_tensor(np.asarray([image,mask]))
    patches = Patches(patch_size)(images)
    image_patches_list = []
    mask_patches_list = []
    for k in [0,1]:
        for i, patch in enumerate(patches[k]): # images
            patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
            if k:
                save_name = "{}_{:02d}_mask.png".format(image_id, i)
                mask_patches_list.append(os.path.join(save_dir, save_name))
            else:
                save_name = "{}_{:02d}_sat.png".format(image_id, i)
                image_patches_list.append(os.path.join(save_dir, save_name))
            tf.keras.utils.save_img(
                path=os.path.join(save_dir, save_name), x=patch_img.numpy().astype("uint8")
            )
    assert len(image_patches_list) == len(mask_patches_list)
    return image_patches_list, mask_patches_list
       

def get_augmentation():
    return A.Compose([
        A.VerticalFlip(p=0.5),              
        A.RandomRotate90(p=0.5)]
    )

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)

# Perform one hot encoding on label
def one_hot_encode(label, label_values):
    """
    Convert a segmentation image label array to one-hot format
    by replacing each pixel value with a vector of length num_classes
    # Arguments
        label: The 2D array segmentation image label
        label_values
        
    # Returns
        A 2D array with the same width and hieght as the input, but
        with a depth size of num_classes
    """
    semantic_map = []
    for colour in label_values:
        equality = np.equal(label, colour)
        class_map = np.all(equality, axis = -1)
        semantic_map.append(class_map)
    semantic_map = np.stack(semantic_map, axis=-1)

    return semantic_map

# Perform reverse one-hot-encoding on labels / preds
def reverse_one_hot(image):
    """
    Transform a 2D array in one-hot format (depth is num_classes),
    to a 2D array with only 1 channel, where each pixel value is
    the classified class key.
    # Arguments
        image: The one-hot format image 
        
    # Returns
        A 2D array with the same width and hieght as the input, but
        with a depth size of 1, where each pixel value is the classified 
        class key.
    """
    x = np.argmax(image, axis = -1)
    return x


class SatelliteImages(tf.keras.utils.Sequence):
    """Helper to iterate over the data (as Numpy arrays)."""

    def __init__(self, batch_size, img_size, 
                 satellite_class_rgb_values, input_img_paths, target_img_paths, 
                 augmentation=None, preprocessing=None):
        self.batch_size = batch_size
        self.img_size = img_size
        self.input_img_paths = input_img_paths
        self.target_img_paths = target_img_paths
        self.augmentation = augmentation
        self.preprocessing = preprocessing
        self.satellite_class_rgb_values = satellite_class_rgb_values
        
        self.forest = np.zeros(self.img_size + (3,), dtype="uint8") + (0,255,0)

    def __len__(self):
        return len(self.target_img_paths) // self.batch_size

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        i = idx * self.batch_size
        batch_input_img_paths = self.input_img_paths[i: i + self.batch_size]
        batch_target_img_paths = self.target_img_paths[i: i + self.batch_size]
        return_batch_length = len(batch_input_img_paths)
        x = np.zeros((return_batch_length,) +
                     self.img_size + (3,), dtype="float32")
        y = np.zeros((return_batch_length,) + self.img_size + (7,), dtype="float32")
        for num in range(return_batch_length):
            image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_input_img_paths[num]), dtype="uint8")
            mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(
                batch_target_img_paths[num]), dtype="uint8")
            
            # mask = (mask == self.forest).all(axis=2,  keepdims=True)
            # one-hot-encode the mask
            mask = one_hot_encode(mask, self.satellite_class_rgb_values).astype('float')
            
            # y[num] = np.expand_dims(mask, 2)
            # apply augmentations
            if self.augmentation:
                sample = self.augmentation(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
            
            # apply preprocessing
            if self.preprocessing:
                sample = self.preprocessing(image=image, mask=mask)
                image, mask = sample['image'], sample['mask']
                
            image = (image / 255.0).astype("float")
            
            x[num] = image
            y[num] = mask

        return x, y
    

def make_model(train, save_directory, model_name, satellite_class_rgb_values):
    if not os.path.exists(save_directory):
        os.mkdir(save_directory)
    
    train_sample = train.sample(n=16*16, random_state=23)
    val_sample = train.sample(n=16*2, random_state=23)
    
    img_size = (256, 256)
    batch_size = 16
    BACKBONE = 'resnet34'
    # BACKBONE = 'efficientnetb3'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    
    # Instantiate data Sequences for each split
    train_gen = SatelliteImages(
        batch_size=batch_size, 
        img_size=img_size, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        input_img_paths=list(train_sample["sat_image_patch"]), 
        target_img_paths=list(train_sample["mask_patch"]),
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )
    val_gen = SatelliteImages(
        batch_size=batch_size, 
        img_size=img_size, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        input_img_paths=list(val_sample["sat_image_patch"]), 
        target_img_paths=list(val_sample["mask_patch"]),
        augmentation=get_augmentation(),
        preprocessing=get_preprocessing(preprocess_input),
    )

    
    model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=7,
                  activation="softmax", encoder_freeze=True)
    # model = sm.Unet(BACKBONE, encoder_weights='imagenet', classes=1,
    #               activation="sigmoid", encoder_freeze=True)
    # define optomizer
    optim = tf.keras.optimizers.Adam(0.001)
    
    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    
    # actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
    # total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss 
    
    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]
    
    # compile keras model with defined optimozer, loss and metrics
    model.compile(optim, total_loss, metrics)
    # model.summary()
   
    callbacks = [
    tf.keras.callbacks.ModelCheckpoint(os.path.join(save_directory, "model.h5"), save_best_only=True),
    tf.keras.callbacks.CSVLogger(os.path.join(save_directory, "model.csv"), append=True)
    ]
    history = model.fit(
        train_gen, 
        epochs=10, 
        callbacks=callbacks, 
        validation_data=val_gen,
        # verbose=2
    )
    
    with open(os.path.join(save_directory, model_name + "_history.pickle"), 'wb') as f:
        pickle.dump(history.history, f)
    return model

    

def load_original_classes():
    DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
    
    class_dict = pd.read_csv(os.path.join(DATASET_DIR, 'class_dict.csv'))
    # Get class names
    satellite_class_list = class_dict['name'].tolist()
    # Get class RGB values
    class_rgb_values = class_dict[['r','g','b']].values.tolist()
        
    # Get RGB values of required classes
    satellite_class_indices = [satellite_class_list.index(cls.lower()) for cls in satellite_class_list]
    satellite_class_rgb_values =  np.array(class_rgb_values)[satellite_class_indices]
    
    return satellite_class_list, satellite_class_indices, satellite_class_rgb_values


def load_original_dataset():
    DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
    metadata_df = pd.read_csv(os.path.join(DATASET_DIR, 'metadata.csv'))
    metadata_df = (metadata_df
                    .query('split in ["train"]')
                    )
    metadata_df['sat_image_path'] = metadata_df['sat_image_path'].apply(
        lambda img_pth: os.path.join(DATASET_DIR, img_pth))
    metadata_df['mask_path'] = metadata_df['mask_path'].apply(
        lambda img_pth: os.path.join(DATASET_DIR, img_pth))
    
    return metadata_df
    
def make_patches(metadata_df):
    PATCHES_DIR = os.path.join("/tmp/", "patches")
    if not os.path.exists(PATCHES_DIR):
        os.mkdir(PATCHES_DIR)

    metadata_patches = []
    count = 0
    with tqdm(total=metadata_df.shape[0]) as pbar:
        for index, row in metadata_df.sample(frac=1).iterrows():  # shuffling
            image_patches_list, mask_patches_list = make_image_cuts(image_id=row["image_id"], image_path=row["sat_image_path"],
                                                                    target_path=row["mask_path"], save_dir=PATCHES_DIR)

            for num in range(len(image_patches_list)):
                entry = dict(row)
                entry["sat_image_patch"] = image_patches_list[num]
                entry["mask_patch"] = mask_patches_list[num]
                metadata_patches.append(entry)

            pbar.update(1)
            count += 1
            # if count > 20:
                # break
            
    metadata_patches_df = pd.DataFrame(metadata_patches)
    metadata_patches_df.to_csv(os.path.join(PATCHES_DIR, "metadata_patches.csv"), index=False)

def load_patches_dataset():
    # PATCHES_DIR = os.path.join("/tmp/", "patches")
    PATCHES_DIR = os.path.join("/home/kent/college/Thesis/deepglobe/", "patches")
    metadata_patches_df = pd.read_csv(
        os.path.join(PATCHES_DIR, 'metadata_patches.csv'))

    # Split the data
    train, test = train_test_split(
        metadata_patches_df, test_size=0.20, shuffle=True)
    # val, test = train_test_split(test, test_size=0.50, shuffle=True)
    return test, train

def load_model(model_path):
    dice_loss = sm.losses.DiceLoss()
    focal_loss = sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)
    m = tf.keras.models.load_model(model_path,
            custom_objects={
                'dice_loss_plus_1focal_loss': total_loss,
                'iou_score': sm.metrics.IOUScore(threshold=0.5),
                'f1-score': sm.metrics.FScore(threshold=0.5)
            })
            
def main():
    test, train = load_patches_dataset()
    satellite_class_list, \
    satellite_class_indices, \
    satellite_class_rgb_values = load_original_classes()
    
    models = []
    # START MODEL MAKING
    for i in range(2,5):
        model_name = "model_{}".format(i)
        print("Train:", model_name)
        save_directory = "/tmp/" + model_name
        model = make_model(train, save_directory, model_name, satellite_class_rgb_values)
        models.append(model)

    test_sample = test.sample(n=20, random_state=23)
    img_size = (256, 256)
    batch_size = 20
    BACKBONE = 'resnet34'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    test_gen = SatelliteImages(
        batch_size=batch_size, 
        img_size=img_size, 
        satellite_class_rgb_values=satellite_class_rgb_values,
        input_img_paths=list(test_sample["sat_image_patch"]), 
        target_img_paths=list(test_sample["mask_patch"]),
        augmentation=None,
        preprocessing=get_preprocessing(preprocess_input),
    )
    
    for m in models:
        # Evaluate the model on the test data
        print("Generate predictions")
        predictions = m.predict(test_gen)
        print("predictions shape:", predictions.shape)
        metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]
        print("Evaluate:")
        scores = m.evaluate(test_gen)
        print("Loss: {:.5}".format(scores[0]))
        for metric, value in zip(metrics, scores[1:]):
            print("mean {}: {:.5}".format(metric.__name__, value))
            
    # LOAD MODELS
    models = []
    for i in [0,2,3,4]:
        model_name = "model_{}".format(i)
        print("Load:", model_name)
        save_directory = "/tmp/" + model_name
        
        dice_loss = sm.losses.DiceLoss()
        focal_loss = sm.losses.CategoricalFocalLoss()
        total_loss = dice_loss + (1 * focal_loss)
        m = tf.keras.models.load_model(os.path.join(save_directory, "{}.h5".format(model_name)),
                                       custom_objects={
                                           'dice_loss_plus_1focal_loss': total_loss,
                                           'iou_score': sm.metrics.IOUScore(threshold=0.5),
                                           'f1-score': sm.metrics.FScore(threshold=0.5)
                                       })
        models.append(m)
        with open(os.path.join(save_directory, model_name + "_history.pickle"), 'rb') as f:
            history = pickle.load(f)
            plt.figure(figsize=(12, 5))
            plt.plot(history['iou_score'])
            plt.plot(history['val_iou_score'])
            plt.title('Model iou_score')
            plt.ylabel('iou_score')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Val'], loc='upper left')
            plt.show()
            
            plt.figure(figsize=(12, 5))
            plt.plot(history['loss'])
            plt.plot(history['val_loss'])
            plt.title('Model loss')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            plt.legend(['Train', 'Val'], loc='upper left')
            plt.show()
            

    predictions = []
    for m in models:
        print("Generate predictions")
        p = m.predict(test_gen)
        print("predictions shape:", p.shape)
        print("Evaluate:")
        scores = m.evaluate(test_gen)
        predictions.append(p)
    predictions = np.stack(predictions)
    
    print("All together:")
    print(predictions.shape)
    
    print("Mean:")
    mean_predictions = np.mean(predictions, axis=(0))
    print(mean_predictions.shape)
    plt.imshow(test_gen[0][0][0,:,:,:])
    plt.show()
    plt.imshow(reverse_one_hot(test_gen[0][1][0,:,:,:]))
    plt.show()
    plt.imshow(reverse_one_hot(mean_predictions[0,:,:,:]))
    plt.show()
    mean_predictions_ints = np.argmax(mean_predictions, axis = -1)
    test_gen_ints = np.argmax(test_gen[0][1], axis = -1)
    
    iou = tf.keras.metrics.MeanIoU(num_classes=7)
    iou.update_state(np.reshape(test_gen_ints, -1),
                     np.reshape(mean_predictions_ints, -1))
    print("IoU:", iou.result().numpy())
    
if __name__ == '__main__':

    pass
    # main()