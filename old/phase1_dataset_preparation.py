#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
1st phase

Data preparation by cutting images into many parts/patches.

"""

import os
import pandas as pd
import tensorflow as tf
from tqdm import tqdm
    
class Patches(tf.keras.layers.Layer):
    def __init__(self, patch_size):
        super(Patches, self).__init__()
        self.patch_size = patch_size

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.patch_size, self.patch_size, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches
    
    
def make_image_cuts(image_id, image_path, target_path, save_dir):
    patch_size = 256  # Size of the patches to be extract from the input images
    image = tf.keras.utils.img_to_array(tf.keras.utils.load_img(image_path))
    mask = tf.keras.utils.img_to_array(tf.keras.utils.load_img(target_path))
    images = tf.convert_to_tensor([image,mask])
    patches = Patches(patch_size)(images)
    image_patches_list = []
    mask_patches_list = []
    for k in [0,1]:
        for i, patch in enumerate(patches[k]): # images
            patch_img = tf.reshape(patch, (patch_size, patch_size, 3))
            if k:
                save_name = "{}_{:02d}_mask.png".format(image_id, i)
                mask_patches_list.append(os.path.join(save_dir, save_name))
            else:
                save_name = "{}_{:02d}_sat.png".format(image_id, i)
                image_patches_list.append(os.path.join(save_dir, save_name))
            tf.keras.utils.save_img(
                path=os.path.join(save_dir, save_name), x=patch_img.numpy().astype("uint8")
            )
    assert len(image_patches_list) == len(mask_patches_list)
    return image_patches_list, mask_patches_list
       
       
if __name__ == '__main__':
    DATASET_DIR = "/home/kent/college/Thesis/deepglobe/"
    metadata_df = pd.read_csv(os.path.join(DATASET_DIR, 'metadata.csv'))
    metadata_df = (metadata_df
                    .query('split in ["train"]')
                    )
    metadata_df['sat_image_path'] = metadata_df['sat_image_path'].apply(
        lambda img_pth: os.path.join(DATASET_DIR, img_pth))
    metadata_df['mask_path'] = metadata_df['mask_path'].apply(
        lambda img_pth: os.path.join(DATASET_DIR, img_pth))
    
    PATCHES_DIR = os.path.join(DATASET_DIR, "patches")
    if not os.path.exists(PATCHES_DIR):
        os.mkdir(PATCHES_DIR)

    metadata_patches = []
    with tqdm(total=metadata_df.shape[0]) as pbar:
        for index, row in metadata_df.iterrows():
            image_patches_list, mask_patches_list = make_image_cuts(image_id=row["image_id"], image_path=row["sat_image_path"],
                            target_path=row["mask_path"], save_dir=PATCHES_DIR)
            
            for num in range(len(image_patches_list)):
                entry = dict(row)
                entry["sat_image_patch"] = image_patches_list[num]
                entry["mask_patch"] = mask_patches_list[num]
                metadata_patches.append(entry)
                
            pbar.update(1)
            if index > 10:
                break
            
    metadata_patches_df = pd.DataFrame(metadata_patches)
    metadata_patches_df.to_csv(os.path.join(PATCHES_DIR, "metadata_patches.csv"), index=False)

            
            
            
            
            
            
            
            
            
            
            
            
            
