#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 15:40:48 2022

@author: kent
"""

import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.utils import load_img, img_to_array

# helper function for data visualization
def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(16, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title())
        plt.imshow(image.astype("uint8"))
    plt.show()
    
image = load_img("/home/kent/college/Thesis/deepglobe/train/147545_sat.jpg")
mask = load_img("/home/kent/college/Thesis/deepglobe/train/147545_mask.png")
mask_gray = load_img("/home/kent/college/Thesis/deepglobe/train/147545_mask.png",color_mode="grayscale")
image.size
mask.size
X = img_to_array(image)
y = img_to_array(mask)
y_gray = img_to_array(mask_gray)

visualize(
    image=X, 
    mask=y,
    mask_gray=y_gray
)
